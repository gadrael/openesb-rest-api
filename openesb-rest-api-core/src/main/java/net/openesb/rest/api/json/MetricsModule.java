package net.openesb.rest.api.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.util.Arrays;
import net.openesb.model.api.Statistic;
import net.openesb.model.api.metric.Gauge;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class MetricsModule extends Module {
    static final Version VERSION = new Version(1, 0, 0, "", "net.open-esb.admin", "rest-api-metrics");

    private static class StatisticSerializer extends StdSerializer<Statistic> {
        private StatisticSerializer() {
            super(Statistic.class);
        }

        @Override
        public void serialize(Statistic statistic,
                              JsonGenerator json,
                              SerializerProvider provider) throws IOException {
            json.writeStartObject();
            try {
                json.writeObjectField("value", statistic.getValue());
                json.writeObjectField("description", statistic.getDescription());
            } catch (RuntimeException e) {
                json.writeObjectField("error", e.toString());
            }
            json.writeEndObject();
        }
    }
    
    private static class GaugeSerializer extends StdSerializer<Gauge> {
        private GaugeSerializer() {
            super(Gauge.class);
        }

        @Override
        public void serialize(Gauge gauge,
                              JsonGenerator json,
                              SerializerProvider provider) throws IOException {
            json.writeStartObject();
            try {
                json.writeObjectField("value", gauge.getValue());
            } catch (RuntimeException e) {
                json.writeObjectField("error", e.toString());
            }
            json.writeEndObject();
        }
    }

    @Override
    public String getModuleName() {
        return "rest-api-metrics";
    }

    @Override
    public Version version() {
        return VERSION;
    }

    @Override
    public void setupModule(SetupContext context) {
        context.addSerializers(new SimpleSerializers(Arrays.<JsonSerializer<?>>asList(
                new StatisticSerializer(),
                new GaugeSerializer()
        )));
    }
}
