package net.openesb.rest.api.provider;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import net.openesb.management.api.ComponentNotFoundException;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Provider
public class ComponentNotFoundExceptionMapper implements ExceptionMapper<ComponentNotFoundException> {

    @Override
    public Response toResponse(ComponentNotFoundException cnfe) {
        return Response.status(Response.Status.NOT_FOUND).entity(cnfe.getMessage()).build();
    }
}
