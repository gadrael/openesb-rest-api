package net.openesb.rest.api.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import net.openesb.management.api.JvmMetricsService;
import net.openesb.rest.api.annotation.RequiresAuthentication;
import net.openesb.rest.api.json.MetricsModule;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Path("jvm")
@RequiresAuthentication
public class JVMInformationsResource {

    @Inject
    private JvmMetricsService jvmMetricsService;

    private static final ObjectMapper mapper = new ObjectMapper().registerModules(
            new MetricsModule());

    @GET
    public Map<String, Object> getInformations() {
        return jvmMetricsService.getInformations();
    }

    @Path("gc")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getGarbageCollectorMetricsResource() throws JsonProcessingException {
        return mapper.writeValueAsString(jvmMetricsService.getGarbageCollector());
    }

    @Path("memory")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getMemoryUsageMetricsResource() throws JsonProcessingException {
        return mapper.writeValueAsString(jvmMetricsService.getMemoryUsage());
    }

    @Path("thread")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getThreadStatesMetricsResource() throws JsonProcessingException {
        return mapper.writeValueAsString(jvmMetricsService.getThreadStates());
    }
}
