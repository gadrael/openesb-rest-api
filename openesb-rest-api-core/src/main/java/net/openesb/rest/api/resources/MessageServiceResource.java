package net.openesb.rest.api.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.NMR;
import net.openesb.management.api.ManagementException;
import net.openesb.management.api.MessageService;
import net.openesb.rest.api.annotation.RequiresAuthentication;
import net.openesb.rest.api.json.MetricsModule;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
@Path("nmr")
@RequiresAuthentication
public class MessageServiceResource extends AbstractResource {
    
    private static final ObjectMapper mapper = new ObjectMapper().registerModules(
            new MetricsModule());
    
    @Inject
    private MessageService messageService;
    
    @Inject
    private ResourceContext resourceContext;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public NMR getMessageService() throws ManagementException {
        return messageService.getNMR();
    }
    
    @Path("stats")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getMessageServiceStatistics() throws ManagementException, JsonProcessingException {
        return mapper.writeValueAsString(messageService.getStatistics());
    }
    
    @Path("endpoints")
    public EndpointsResource getEndpointsResource() {
        return resourceContext.initResource(new EndpointsResource());
    }
    
    @Path("{component}")
    public ComponentResource getComponentResource(@PathParam("component") String componentName) {
        return resourceContext.initResource(
                new ComponentResource(componentName));
    }
}
