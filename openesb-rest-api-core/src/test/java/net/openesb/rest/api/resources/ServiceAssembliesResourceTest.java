package net.openesb.rest.api.resources;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.MediaType;
import net.openesb.model.api.ServiceAssembly;
import net.openesb.model.api.State;
import net.openesb.management.api.ManagementException;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class ServiceAssembliesResourceTest extends AbstractResourceTest {
    
    @Test
    public void listEmptyServiceAssemblies() {
        Set<ServiceAssembly> assemblies = target("assemblies")
                .request().accept(MediaType.APPLICATION_JSON_TYPE)
                .get(Set.class);
        
        Assert.assertNotNull(assemblies);
    }
    
    @Test
    public void listServiceAssemblies() throws ManagementException {
        Mockito.when(
                serviceAssemblyServiceMock.findServiceAssemblies(null, null)).
                thenReturn(constructServiceAssemblies());
        
        Set<ServiceAssembly> assemblies = target("assemblies")
                .request().accept(MediaType.APPLICATION_JSON_TYPE)
                .get(Set.class);
        
        Assert.assertTrue(! assemblies.isEmpty()); 
   }
    
    private Set<ServiceAssembly> constructServiceAssemblies() {
        Set<ServiceAssembly> assemblies = new HashSet<ServiceAssembly>();
        
        ServiceAssembly assembly1 = new ServiceAssembly();
        assembly1.setName("Assembly1");
        assembly1.setState(State.UNKNOWN);
        
        assemblies.add(assembly1);
        
        return assemblies;
    }
}
